package org.yaml.jury;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Test_2_2_Structures
{
	@Test
	public void Example2_7() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.7.Two_documents_in_a_stream.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Sequence document = reader.readDocument().asSequence();
			assertNotNull(document);
			assertEquals("Mark McGwire", document.getScalar(0).toString());
			assertEquals("Sammy Sosa", document.getScalar(1).toString());
			assertEquals("Ken Griffey", document.getScalar(2).toString());
			assertEquals(3, document.size());

			document = reader.readDocument().asSequence();
			assertNotNull(document);
			assertEquals("Chicago Cubs", document.getScalar(0).toString());
			assertEquals("St Louis Cardinals", document.getScalar(1).toString());
			assertEquals(2, document.size());
		}
	}

	@Test
	public void Example2_8() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.8.Play_by_play_feed.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Mapping document = reader.readDocument().asMapping();
			assertNotNull(document);
			assertEquals("20:03:20", document.getScalar("time").toString());
			assertEquals("Sammy Sosa", document.getScalar("player").toString());
			assertEquals("strike (miss)", document.getScalar("action").toString());
			assertEquals(3, document.size());

			document = reader.readDocument().asMapping();
			assertNotNull(document);
			assertEquals("20:03:47", document.getScalar("time").toString());
			assertEquals("Sammy Sosa", document.getScalar("player").toString());
			assertEquals("grand slam", document.getScalar("action").toString());
			assertEquals(3, document.size());
		}
	}

	private void hrRbi(InputStream input)
	{
		YamlReader reader = new YamlReader(input);
		Mapping document = reader.readDocument().asMapping();
		assertNotNull(document);
		Sequence hr = document.getSequence("hr");
		assertEquals("Mark McGwire", hr.getScalar(0).toString());
		assertEquals("Sammy Sosa", hr.getScalar(1).toString());
		assertEquals(2, hr.size());
		Sequence rbi = document.getSequence("rbi");
		assertEquals("Sammy Sosa", rbi.getScalar(0).toString());
		assertEquals("Ken Griffey", rbi.getScalar(1).toString());
		assertEquals(2, rbi.size());
		assertEquals(2, document.size());
	}

	@Test
	public void Example2_9() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.9.Single_document_with_two_comments.yml"))
		{
			hrRbi(input);
		}
	}

	@Test
	public void Example2_10() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.10.Node_for_Sammy_Sosa_appears_twice.yml"))
		{
			hrRbi(input);
		}
	}

	private void playSchedule(InputStream input, boolean capitalCubs)
	{
		YamlReader reader = new YamlReader(input);
		Mapping document = reader.readDocument().asMapping();
		assertNotNull(document);
		Sequence key1 = new Sequence();
		key1.add(new Scalar("Detroit Tigers"));
		key1.add("Chicago " + (capitalCubs ? "Cubs" : "cubs"));
		Sequence value1 = document.getSequence(key1);
		assertEquals("2001-07-23", value1.getScalar(0).toString());
		assertEquals(1, value1.size());
		Sequence key2 = new Sequence();
		key2.add("New York Yankees");
		key2.add(new Scalar("Atlanta Braves"));
		Sequence value2 = document.getSequence(key2);
		assertEquals("2001-07-02", value2.getScalar(0).toString());
		assertEquals("2001-08-12", value2.getScalar(1).toString());
		assertEquals("2001-08-14", value2.getScalar(2).toString());
		assertEquals(3, value2.size());
		assertEquals(2, document.size());
	}

	@Test
	public void Example2_11() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.11.Mapping_between_sequences.yml"))
		{
			playSchedule(input, true);
		}
	}

	@Test
	public void Example2_11_with_comment() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_2.11.Mapping_between_sequences.yml"))
		{
			playSchedule(input, false);
		}
	}

	@Test
	public void Example2_12() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.12.Sequence_key_shortcut.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Sequence document = reader.readDocument().asSequence();
			assertNotNull(document);
			assertEquals("Super Hoop", document.getMapping(0).getScalar("item").toString());
			assertEquals("1", document.getMapping(0).getScalar("quantity").toString());
			assertEquals(2, document.getCollection(0).size());
			assertEquals("Basketball", document.getMapping(1).getScalar("item").toString());
			assertEquals("4", document.getMapping(1).getScalar("quantity").toString());
			assertEquals(2, document.getCollection(1).size());
			assertEquals("Big Shoes", document.getMapping(2).getScalar("item").toString());
			assertEquals("1", document.getMapping(2).getScalar("quantity").toString());
			assertEquals(2, document.getCollection(2).size());
			assertEquals(3, document.size());
		}
	}

	@Test
	public void Example2_13() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.13.In_literals_newlines_are_preserved.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Scalar document = reader.readDocument().asScalar();
			assertNotNull(document);
			assertEquals("\\//||\\/||\n// ||  ||__", document.toString());
		}
	}

	@Test
	public void Example2_14_no_indicator() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.14.In_the_plain_scalar_newlines_are_treated_as_a_space.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Scalar document = reader.readDocument().asScalar();
			assertNotNull(document);
			assertEquals("Mark McGwire's year was crippled by a knee injury.", document.toString());
		}
	}

	@Test
	public void Example2_14_with_indicator() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.2/Example_2.14.In_the_folded_scalars_newlines_becoe_spaces.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Scalar document = reader.readDocument().asScalar();
			assertNotNull(document);
			assertEquals("Mark McGwire's year was crippled by a knee injury.", document.toString());
		}
	}

	@Test
	public void Example2_15_document_start() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.15.Folded_newlines_preserved.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Scalar document = reader.readDocument().asScalar();
			assertNotNull(document);
			assertEquals("Sammy Sosa completed another fine season with great stats.\n\n  63 Home Runs\n  0.288 Batting Average\n\nWhat a year!", document.toString());
		}
	}

	@Test
	public void Example2_15_plain() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_2.15.Folded_newlines_are_preserved.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Scalar document = reader.readDocument().asScalar();
			assertNotNull(document);
			assertEquals("Sammy Sosa completed another fine season with great stats.\n\n  63 Home Runs\n  0.288 Batting Average\n\nWhat a year!", document.toString());
		}
	}

	@Test
	public void Example2_16() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.16.Indentation_determines_scope.yml"))
		{
			YamlReader reader = new YamlReader(input);
			Mapping document = reader.readDocument().asMapping();
			assertNotNull(document);
			assertEquals("Mark McGwire", document.getScalar("name").toString());
			assertEquals("Mark set a major league home run record in 1998.\n", document.getScalar("accomplishment").toString());
			assertEquals("65 Home Runs\n0.278 Batting Average\n", document.getScalar("stats").toString());
			assertEquals(3, document.size());
		}
	}
}
