package org.yaml.jury;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class Test_Yaml_Multiline
{
	@Test
	public void Flow_Structure() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-flow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(new Scalar("Single-quoted"), new Scalar("Double-quoted"), new Scalar("Plain")));
			assertEquals(3, document.size());
		}
	}

	@Test
	public void Flow_Single_Quoted() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-flow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text, containing 'single quotes'. Escapes (like \\n) don't do anything.\n" +
					"Newlines can be added by leaving a blank line. Leading whitespace on lines is ignored.", document.getScalar("Single-quoted").toString());
		}
	}

	@Test
	public void Flow_Double_Quoted () throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-flow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			// https://yaml-multiline.info/ says this scalar should have trailing newline, but i can't find any reason in the spec why it should.
			// https://yaml-online-parser.appspot.com/ also does not have a newline at the end of this scalar.
			assertEquals("Several lines of text, containing \"double quotes\". Escapes (like \\n) work.\n" +
					"In addition, newlines can be escaped to prevent them from being converted to a space.\n" +
					"Newlines can also be added by leaving a blank line. Leading whitespace on lines is ignored.", document.getScalar("Double-quoted").toString());
		}
	}

	@Test
	public void Flow_Plain() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-flow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text, with some \"quotes\" of various 'types'. Escapes (like \\n) don't do anything.\n" +
					"Newlines can be added by leaving a blank line. Additional leading whitespace is ignored.", document.getScalar("Plain").toString());
		}
	}

	@Test
	public void Block_Structure() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-block.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			for (String style : new String[]{"folded", "literal"})
			{
				for (String chomping : new String[]{"clip", "strip", "keep"})
				{
					String key = style + "-" + chomping;
					assertTrue("Missing key: " + key, document.keySet().contains(new Scalar(key)));
					assertNotNull("Node is null: " + key, document.get(key));
					assertTrue("Node " + key + " should be scalar but is " + document.get(key).getClass(), document.get(key).isScalar());
				}
			}
			assertEquals(6, document.size());
		}
	}

	@Test
	public void Block_Folded_Keep() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-block.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text, with some \"quotes\" of various 'types', and also a blank line:\n" +
					"plus another line at the end.\n\n\n", document.getScalar("folded-keep").toString());
		}
	}

	@Test
	public void Block_Folded_Clip() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-block.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text, with some \"quotes\" of various 'types', and also a blank line:\n" +
					"plus another line at the end.\n", document.getScalar("folded-clip").toString());
		}
	}

	@Test
	public void Block_Folded_Strip() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-block.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text, with some \"quotes\" of various 'types', and also a blank line:\n" +
					"plus another line at the end.", document.getScalar("folded-strip").toString());
		}
	}

	@Test
	public void Block_Literal_Keep() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-block.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text,\n" +
					"with some \"quotes\" of various 'types',\n" +
					"and also a blank line:\n\n" +
					"plus another line at the end.\n\n\n", document.getScalar("literal-keep").toString());
		}
	}

	@Test
	public void Block_Literal_Clip() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-block.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text,\n" +
					"with some \"quotes\" of various 'types',\n" +
					"and also a blank line:\n\n" +
					"plus another line at the end.\n", document.getScalar("literal-clip").toString());
		}
	}

	@Test
	public void Block_Literal_Strip() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml-multiline-block.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("Several lines of text,\n" +
					"with some \"quotes\" of various 'types',\n" +
					"and also a blank line:\n\n" +
					"plus another line at the end.", document.getScalar("literal-strip").toString());
		}
	}
}
