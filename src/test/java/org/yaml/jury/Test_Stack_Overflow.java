package org.yaml.jury;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class Test_Stack_Overflow
{
	@Test
	public void Structure() throws IOException
	{
		try (InputStream input = Resources.getFile("/stack-overflow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(
				new Scalar("Folded"),
				new Scalar("Literal"),
				new Scalar("Plain"),
				new Scalar("DoubleQuoted"),
				new Scalar("SingleQuoted"),
				new Scalar("examples"),
				new Scalar("with indentation"),
				new Scalar("addendum")
			));
			assertEquals(8, document.size());

			assertTrue(document.get("examples").isSequence());
			assertEquals(6, document.get("examples").asSequence().size());
			assertTrue(document.get("with indentation").isSequence());
			assertEquals(2, document.get("with indentation").asSequence().size());
			assertTrue(document.get("addendum").isSequence());
			assertEquals(2, document.get("addendum").asSequence().size());
		}
	}

	@Test
	public void BasicStyles() throws IOException
	{
		try (InputStream input = Resources.getFile("/stack-overflow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("this is my very very very long string\n", document.getScalar("Folded").toString());
			assertEquals("this is my very very very\nlong string\n", document.getScalar("Literal").toString());
			assertEquals("this is my very very very long string", document.getScalar("Plain").toString());
			assertEquals("this is my very very \"very\" loooong string.\n\nLove, YAML.", document.getScalar("DoubleQuoted").toString());
			assertEquals("this is my very very \"very\" long string, isn't it.", document.getScalar("SingleQuoted").toString());
		}
	}

	@Test
	public void Examples() throws IOException
	{
		try (InputStream input = Resources.getFile("/stack-overflow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			Sequence examples = document.getSequence("examples");
			assertEquals("very \"long\" 'string' with\nparagraph gap, \\n and         spaces.\n", examples.getScalar(0).toString());
			assertEquals("very \"long\"\n'string' with\n\nparagraph gap, \\n and        \nspaces.\n", examples.getScalar(1).toString());
			assertEquals("very \"long\" 'string' with\nparagraph gap, \\n and spaces.", examples.getScalar(2).toString());
			assertEquals("very \"long\" 'string' with\nparagraph gap, \n and spaces.", examples.getScalar(3).toString());
			assertEquals("very \"long\" 'string' with\nparagraph gap, \\n and spaces.", examples.getScalar(4).toString());
			assertEquals("very \"long\" 'string' with\nparagraph gap, \\n and         spaces.", examples.getScalar(5).toString());
		}
	}

	@Test
	public void WithIndentation() throws IOException
	{
		try (InputStream input = Resources.getFile("/stack-overflow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			Sequence indent = document.getSequence("with indentation");
			assertEquals("My long string starts over here\n", indent.getScalar(0).toString());
			assertEquals("This one\nstarts here\n", indent.getScalar(1).toString());
		}
	}

	@Test
	public void Addendum() throws IOException
	{
		try (InputStream input = Resources.getFile("/stack-overflow.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			Sequence addendum = document.getSequence("addendum");
			assertEquals("my long\n  string\n", addendum.getScalar(0).toString());
			assertEquals("my long string", addendum.getScalar(1).toString());
		}
	}
}
