package org.yaml.jury;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class Test_2_3_Scalars
{
	@Test
	public void Example2_17_1_0() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.17.Quoted_Scalars.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(new Scalar("unicode"), new Scalar("control"), new Scalar("hexesc"), new Scalar("single"), new Scalar("quoted"), new Scalar("tie-fighter")));
			assertEquals("Sosa did fine.\u263A", document.getScalar("unicode").toString());
			assertEquals("\b1998\t1999\t2000\n", document.getScalar("control").toString());
			assertEquals("\u0013\u0010 is \r\n", document.getScalar("hexesc").toString());
			assertEquals("\"Howdy!\" he cried.", document.getScalar("single").toString());
			assertEquals(" # not a 'comment'.", document.getScalar("quoted").toString());
			assertEquals("|\\-*-/|", document.getScalar("tie-fighter").toString());
			assertEquals(6, document.size());
		}
	}

	@Test
	public void Example2_17_1_2() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.2/Example_2.17.Quoted_Scalars.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(new Scalar("unicode"), new Scalar("control"), new Scalar("hex esc"), new Scalar("single"), new Scalar("quoted"), new Scalar("tie-fighter")));
			assertEquals("Sosa did fine.\u263A", document.getScalar("unicode").toString());
			assertEquals("\b1998\t1999\t2000\n", document.getScalar("control").toString());
			assertEquals("\r\n is \r\n", document.getScalar("hex esc").toString());
			assertEquals("\"Howdy!\" he cried.", document.getScalar("single").toString());
			assertEquals(" # Not a 'comment'.", document.getScalar("quoted").toString());
			assertEquals("|\\-*-/|", document.getScalar("tie-fighter").toString());
			assertEquals(6, document.size());
		}
	}

	@Test
	public void Example2_18() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.18.Multiline_flow_scalars.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(new Scalar("plain"), new Scalar("quoted")));
			assertEquals("This unquoted scalar spans many lines.", document.getScalar("plain").toString());
			assertEquals("So does this quoted scalar.\n", document.getScalar("quoted").toString());
			assertEquals(2, document.size());
		}
	}
}