package org.yaml.jury;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Test_5_3_Indicator_Characters
{
	@Test
	public void Example5_3() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_5.3.Block_structure_indicators.yml"))
		{
			oneTwoSeaBlue(input);
		}
	}

	private Mapping oneTwoSeaBlue(InputStream input)
	{
		Mapping document = new YamlReader(input).readDocument().asMapping();
		assertNotNull(document);
		Sequence block = document.getSequence("sequence");
		assertNotNull(block);
		assertEquals("one", block.getScalar(0).toString());
		assertEquals("two", block.getScalar(1).toString());
		assertEquals(2, block.size());
		Mapping map = document.getMapping("mapping");
		assertNotNull(map);
		assertEquals("blue", map.getScalar("sky").toString());
		assertEquals("green", map.getScalar("sea").toString());
		assertEquals(2, map.size());
		assertEquals(2, document.size());
		return document;
	}

	@Test
	public void Example5_3_explicit() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_5.3.Block_structure_indicators_explicit.yml"))
		{
			Mapping document = oneTwoSeaBlue(input);
			assertEquals("!!map", document.getTag());
			for (Node key : document.keySet())
			{
				assertEquals("!!str", key.getTag());
			}
			assertEquals("!!seq", document.get("sequence").getTag());
			for (Node value : document.getSequence("sequence"))
			{
				assertEquals("!!str", value.getTag());
			}
			assertEquals("!!map", document.get("mapping").getTag());
			for (Node key : document.getMapping("mapping").keySet())
			{
				assertEquals("!!str", key.getTag());
				assertEquals("!!str", document.getMapping("mapping").get(key).getTag());
			}
		}
	}

	@Test
	public void Example5_4() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_5.4.Flow_collection_indicators.yml"))
		{
			oneTwoSeaBlue(input);
		}
	}

	@Test
	public void Example5_5() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_5.5.Comment_indicator.yml"))
		{
			Scalar document = new YamlReader(input).readDocument().asScalar();
			assertNotNull(document);
			assertEquals("", document.toString());
		}
	}
}
