package org.yaml.jury;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class Test_Elements
{
	@Test
	public void ScalarContent()
	{
		StringBuilder sb = new StringBuilder("Builder Content");
		Node sbNode = new Scalar(sb);
		Node nullNode = new Scalar((String) null);
		assertEquals(0, nullNode.hashCode());
		assertTrue("Scalar with content equality", sbNode.equals(sbNode));
		assertTrue("Scalar without content equality", nullNode.equals(nullNode));
		assertFalse("Scalar mixed null equality", sbNode.equals(nullNode));
		assertFalse("Scalar mixed null equality", nullNode.equals(sbNode));
		assertFalse("Scalar other object equality", sbNode.equals(new Object()));
		assertEquals(0, nullNode.asScalar().asInt());
		assertTrue(Double.isNaN(nullNode.asScalar().asDouble()));
	}

	@Test
	public void ScalarConversions()
	{
		Node node = new Scalar("Test");
		assertFalse("Scalar convertible to collection", node.isCollection());
		assertTrue("Scalar convertible to scalar", node.isScalar());
		assertFalse("Scalar convertible to sequence", node.isSequence());
		assertFalse("Scalar convertible to mapping", node.isMapping());

		assertNull("Scalar conversion to collection", node.asCollection());
		assertEquals(node, node.asScalar());
		assertNull("Scalar conversion to sequence", node.asSequence());
		assertNull("Scalar conversion to mapping", node.asMapping());
	}

	@Test
	public void SequenceConversions()
	{
		Node node = new Sequence();
		assertTrue("Sequence convertible to collection", node.isCollection());
		assertFalse("Sequence convertible to scalar", node.isScalar());
		assertTrue("Sequence convertible to sequence", node.isSequence());
		assertFalse("Sequence convertible to mapping", node.isMapping());

		assertEquals(node, node.asCollection());
		assertNull("Sequence conversion to scalar", node.asScalar());
		assertEquals(node, node.asSequence());
		assertNull("Sequence conversion to mapping", node.asMapping());
	}

	@Test
	public void MappingConversions()
	{
		Node node = new Mapping();
		assertTrue("Mapping convertible to collection", node.isCollection());
		assertFalse("Mapping convertible to scalar", node.isScalar());
		assertFalse("Mapping convertible to sequence", node.isSequence());
		assertTrue("Mapping convertible to mapping", node.isMapping());

		assertEquals(node, node.asCollection());
		assertNull("Mapping conversion to scalar", node.asScalar());
		assertNull("Mapping conversion to sequence", node.asSequence());
		assertEquals(node, node.asMapping());
	}

	@Test
	public void SequenceOperations()
	{
		Scalar scalarOne = new Scalar("one");
		Scalar scalarTwo = new Scalar("two");
		Sequence seq = new Sequence();

		int seq0Hash = seq.hashCode();
		seq.add(scalarOne);
		int seq1Hash = seq.hashCode();
		seq.add((Node)null);
		int seq2Hash = seq.hashCode();
		// Recursive sequence, yes, because some men just want to see the world burn
		seq.add(seq);
		int seq3Hash = seq.hashCode();
		assertTrue("Sequence hashcode by content", seq0Hash != seq1Hash);
		assertTrue("Sequence hashcode by content", seq0Hash != seq2Hash);
		assertTrue("Sequence hashcode by content", seq0Hash != seq3Hash);
		assertTrue("Sequence hashcode by content", seq1Hash != seq2Hash);
		assertTrue("Sequence hashcode by content", seq1Hash != seq3Hash);
		assertTrue("Sequence hashcode by content", seq2Hash != seq3Hash);

		assertFalse("Sequence equality", seq.equals(scalarOne));
	}

	@Test
	public void MappingOperations()
	{
		Scalar scalarOne = new Scalar("one");
		Scalar scalarTwo = new Scalar("two");
		Mapping map = new Mapping();

		int map0Hash = map.hashCode();
		map.add(scalarOne, scalarTwo);
		int map1Hash = map.hashCode();
		map.add(scalarTwo, null);
		int map2Hash = map.hashCode();
		map.add(null, scalarTwo);
		int map3Hash = map.hashCode();
		assertTrue("Mapping hashcode by content", map0Hash != map1Hash);
		assertTrue("Mapping hashcode by content", map0Hash != map2Hash);
		assertTrue("Mapping hashcode by content", map0Hash != map3Hash);
		assertTrue("Mapping hashcode by content", map1Hash != map2Hash);
		assertTrue("Mapping hashcode by content", map1Hash != map3Hash);
		assertTrue("Mapping hashcode by content", map2Hash != map3Hash);
	}
}