package org.yaml.jury;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;
import static org.yaml.jury.Resources.*;

public class Test_2_1_Collections
{
	@Test
	public void Example2_1() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.1.Sequence_of_scalars.yml"))
		{
			Sequence document = new YamlReader(input).readDocument().asSequence();
			assertNotNull(document);
			assertTrue(document.get(0).isScalar());
			assertTrue(document.get(1).isScalar());
			assertTrue(document.get(2).isScalar());
			assertEquals("Mark McGwire", document.getScalar(0).toString());
			assertEquals("Sammy Sosa", document.getScalar(1).toString());
			assertEquals("Ken Griffey", document.getScalar(2).toString());
			assertEquals(3, document.size());
		}
	}

	@Test
	public void Example2_2() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.2.Mapping_of_scalars_to_scalars.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(HR, RBI, AVG));
			assertEquals("65", document.getScalar("hr").toString());
			assertEquals("0.278", document.getScalar("avg").toString());
			assertEquals("147", document.getScalar("rbi").toString());
			assertEquals(3, document.size());
		}
	}

	@Test
	public void Example2_2_with_comments() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_2.2.Mapping_of_scalars_to_scalars.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(HR, RBI, AVG));
			assertEquals("65", document.getScalar("hr").toString());
			assertEquals("0.278", document.getScalar("avg").toString());
			assertEquals("147", document.getScalar("rbi").toString());
			assertEquals(3, document.size());
		}
	}

	@Test
	public void Example2_3() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.3.Mapping_of_scalars_to_sequences.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(AMERICAN, NATIONAL));
			assertTrue(document.get(AMERICAN).isSequence());
			assertTrue(document.get(NATIONAL).isSequence());
			Sequence american = document.getSequence(AMERICAN);
			assertEquals("Boston Red Sox", american.getScalar(0).toString());
			assertEquals("Detroit Tigers", american.getScalar(1).toString());
			assertEquals("New York Yankees", american.getScalar(2).toString());
			assertEquals(3, american.size());
			Sequence national = document.getSequence(NATIONAL);
			assertEquals("New York Mets", national.getScalar(0).toString());
			assertEquals("Chicago Cubs", national.getScalar(1).toString());
			assertEquals("Atlanta Braves", national.getScalar(2).toString());
			assertEquals(3, national.size());
			assertEquals(2, document.size());
		}
	}

	@Test
	public void Example2_4() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.4.Sequence_of_mappings.yml"))
		{
			Sequence document = new YamlReader(input).readDocument().asSequence();
			assertNotNull(document);
			Mapping map = document.get(0).asMapping();
			assertNotNull(map);
			assertThat(map.keySet(), CoreMatchers.hasItems(NAME, HR, AVG));
			assertEquals("Mark McGwire", map.get("name").asScalar().toString());
			assertEquals("65", map.get("hr").asScalar().toString());
			assertEquals("0.278", map.get("avg").asScalar().toString());
			assertEquals(3, map.size());
			map = document.get(1).asMapping();
			assertNotNull(map);
			assertThat(map.keySet(), CoreMatchers.hasItems(NAME, HR, AVG));
			assertEquals("Sammy Sosa", map.get(NAME).asScalar().toString());
			assertEquals("63", map.get(HR).asScalar().toString());
			assertEquals("0.288", map.get(AVG).asScalar().toString());
			assertEquals(3, map.size());
			assertEquals(2, document.size());
		}
	}

	@Test
	public void Example2_5() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.5.Sequence_of_sequences.yml"))
		{
			Sequence document = new YamlReader(input).readDocument().asSequence();
			assertNotNull(document);
			for (int i = 0; i < document.size(); i++)
			{
				assertTrue(document.get(i).isSequence());
				assertEquals(3, document.get(i).asCollection().size());
			}
			assertEquals("name", document.get(0).asSequence().get(0).asScalar().toString());
			assertEquals("hr", document.get(0).asSequence().get(1).asScalar().toString());
			assertEquals("avg", document.get(0).asSequence().get(2).asScalar().toString());
			assertEquals("Mark McGwire", document.get(1).asSequence().get(0).asScalar().toString());
			assertEquals("65", document.get(1).asSequence().get(1).asScalar().toString());
			assertEquals("0.278", document.get(1).asSequence().get(2).asScalar().toString());
			assertEquals("Sammy Sosa", document.get(2).asSequence().get(0).asScalar().toString());
			assertEquals("63", document.get(2).asSequence().get(1).asScalar().toString());
			assertEquals("0.288", document.get(2).asSequence().get(2).asScalar().toString());
			assertEquals(3, document.size());
		}
	}

	@Test
	public void Example2_6() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.6.Mapping_of_mappings.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertThat(document.keySet(), CoreMatchers.hasItems(MARK_MCGWIRE, SAMMY_SOSA));
			Mapping mark = document.getMapping(MARK_MCGWIRE);
			assertNotNull(mark);
			assertThat(mark.keySet(), CoreMatchers.hasItems(HR, AVG));
			assertEquals("65", mark.getScalar(HR).toString());
			assertEquals("0.278", mark.getScalar(AVG).toString());
			assertEquals(2, mark.size());
			Mapping sammy = document.get(SAMMY_SOSA).asMapping();
			assertNotNull(sammy);
			assertThat(sammy.keySet(), CoreMatchers.hasItems(AVG, HR));
			assertEquals("63", sammy.get(HR).asScalar().toString());
			assertEquals("0.288", sammy.get(AVG).asScalar().toString());
			assertEquals(2, sammy.size());
			assertEquals(2, document.size());
		}
	}
}
