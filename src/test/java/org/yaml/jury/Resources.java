package org.yaml.jury;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

import static org.junit.Assert.fail;

public class Resources
{
	public static final Node NAME = new Scalar("name");
	public static final Node HR = new Scalar("hr");
	public static final Node RBI = new Scalar("rbi");
	public static final Node AVG = new Scalar("avg");
	public static final Node AMERICAN = new Scalar("american");
	public static final Node NATIONAL = new Scalar("national");
	public static final Node MARK_MCGWIRE = new Scalar("Mark McGwire");
	public static final Node SAMMY_SOSA = new Scalar("Sammy Sosa");

	public static InputStream getFile(String name) throws FileNotFoundException
	{
		URL url = Resources.class.getResource(name);
		if (url == null)
		{
			fail("Missing resource: " + name);
		}
		return new FileInputStream(url.getFile());
	}
}
