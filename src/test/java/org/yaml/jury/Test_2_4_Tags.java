package org.yaml.jury;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * According to the Yaml specification untagged nodes are given a type depending on the application.
 * Since this is application specific and JuRY aims to be a parser only, no implicit tags are assigned.
 * The data will be available as normal scalars which the application can interpret any way it sees fit.
 */
public class Test_2_4_Tags
{
	@Test
	public void Example2_19_1_0() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.19.Integers.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("12345", document.getScalar("canonical").toString());
			assertEquals("+12,345", document.getScalar("decimal").toString());
			assertEquals("3:25:45", document.getScalar("sexagecimal").toString());
			assertEquals("014", document.getScalar("octal").toString());
			assertEquals("0xC", document.getScalar("hexadecimal").toString());
		}
	}

	@Test
	public void Example2_19_1_2() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.2/Example_2.19.Integers.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("12345", document.getScalar("canonical").toString());
			assertEquals("+12345", document.getScalar("decimal").toString());
			assertEquals("0o14", document.getScalar("octal").toString());
			assertEquals("0xC", document.getScalar("hexadecimal").toString());
		}
	}

	@Test
	public void Example2_20_1_0() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.20.Floating_point.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("1.23015e+3", document.getScalar("canonical").toString());
			assertEquals("12.3015e+02", document.getScalar("exponential").toString());
			assertEquals("20:30.15", document.getScalar("sexagecimal").toString());
			assertEquals("1,230.15", document.getScalar("fixed").toString());
			assertEquals("(-inf)", document.getScalar("negative infinity").toString());
			assertEquals("(NaN)", document.getScalar("not a number").toString());
		}
	}

	@Test
	public void Example2_20_1_1() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_2.20.Floating_point.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("1.23015e+3", document.getScalar("canonical").toString());
			assertEquals("12.3015e+02", document.getScalar("exponential").toString());
			assertEquals("20:30.15", document.getScalar("sexagesimal").toString());
			assertEquals("1,230.15", document.getScalar("fixed").toString());
			assertEquals("-.inf", document.getScalar("negative infinity").toString());
			assertEquals(".NaN", document.getScalar("not a number").toString());
		}
	}

	@Test
	public void Example2_21_1_0() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.21.Miscellaneous.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("~", document.getScalar("null").toString());
			assertEquals("y", document.getScalar("true").toString());
			assertEquals("n", document.getScalar("false").toString());
			assertEquals("12345", document.getScalar("string").toString());
		}
	}

	@Test
	public void Example2_21_1_2() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.2/Example_2.21.Miscellaneous.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNull(document.getScalar("null"));
			assertEquals("true", document.getSequence("booleans").getScalar(0).toString());
			assertEquals("false", document.getSequence("booleans").getScalar(1).toString());
			assertEquals("012345", document.getScalar("string").toString());
		}
	}

	@Test
	public void Example2_22_1_0() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.22.Timestamps.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("2001-12-15T02:59:43.1Z", document.getScalar("canonical").toString());
			assertEquals("2001-12-14t21:59:43.10-05:00", document.getScalar("iso8601").toString());
			assertEquals("2001-12-14 21:59:43.10 -05:00", document.getScalar("spaced").toString());
			assertEquals("2002-12-14", document.getScalar("date").toString());
		}
	}

	@Test
	public void Example2_22_1_1() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_2.22.Timestamps.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("2001-12-15T02:59:43.1Z", document.getScalar("canonical").toString());
			assertEquals("2001-12-14t21:59:43.10-05:00", document.getScalar("iso8601").toString());
			assertEquals("2001-12-14 21:59:43.10 -5", document.getScalar("spaced").toString());
			assertEquals("2002-12-14", document.getScalar("date").toString());
		}
	}

	@Test
	public void Example2_23_1_0() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.23.Various_explicit_tags.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertEquals("2002-04-28", document.getScalar("not-date").toString());
			assertEquals("R0lGODlhDAAMAIQAAP//9/X\n17unp5WZmZgAAAOfn515eXv\nPz7Y6OjuDg4J+fn5OTk6enp\n56enmleECcgggoBADs=\n", document.getScalar("picture").toString());
			assertEquals("The semantics of the tag\nabove may be different for\ndifferent documents.\n", document.getScalar("application specific tag").toString());

			assertEquals("!str", document.get("not-date").getTag());
			assertEquals("!binary", document.get("picture").getTag());
			assertEquals("!!something", document.get("application specific tag").getTag());
		}
	}

	@Test
	public void Example2_23_1_1() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.1/Example_2.23.Various_explicit_tags.yml"))
		{
			Mapping document = new YamlReader(input).readDocument(true).asMapping();
			assertEquals("2002-04-28", document.getScalar("not-date").toString());
			assertEquals("R0lGODlhDAAMAIQAAP//9/X\n17unp5WZmZgAAAOfn515eXv\nPz7Y6OjuDg4J+fn5OTk6enp\n56enmleECcgggoBADs=\n", document.getScalar("picture").toString());
			assertEquals("The semantics of the tag\nabove may be different for\ndifferent documents.\n", document.getScalar("application specific tag").toString());

			assertEquals("!!str", document.get("not-date").getTag());
			assertEquals("!!binary", document.get("picture").getTag());
			assertEquals("!something", document.get("application specific tag").getTag());
		}
	}

	@Test
	public void Example2_24_1_0() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_2.24.Application_specific_tag.yml"))
		{
			Sequence document = new YamlReader(input).readDocument(true).asSequence();
			Mapping circle = document.getMapping(0);
			assertEquals("73", circle.getMapping("center").getScalar("x").toString());
			assertEquals("129", circle.getMapping("center").getScalar("y").toString());
			assertEquals("7", circle.getScalar("radius").toString());
			Mapping line = document.getMapping(1);
			assertEquals("73", line.getMapping("start").getScalar("x").toString());
			assertEquals("129", line.getMapping("start").getScalar("y").toString());
			assertEquals("89", line.getMapping("finish").getScalar("x").toString());
			assertEquals("102", line.getMapping("finish").getScalar("y").toString());
			Mapping label = document.getMapping(2);
			assertEquals("73", label.getMapping("start").getScalar("x").toString());
			assertEquals("129", label.getMapping("start").getScalar("y").toString());
			assertEquals("0xFFEEBB", label.getScalar("color").toString());
			assertEquals("Pretty vector drawing.", label.getScalar("value").toString());

			assertEquals("!clarkevans.com,2002/graph/circle", document.getMapping(0).getTag());
			assertEquals("!clarkevans.com,2002/graph/line", document.getMapping(1).getTag());
			assertEquals("!clarkevans.com,2002/graph/label", document.getMapping(2).getTag());
			assertEquals("!clarkevans.com,2002/graph/shape", document.getTag());
		}
	}
}
