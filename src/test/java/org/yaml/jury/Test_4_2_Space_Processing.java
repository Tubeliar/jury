package org.yaml.jury;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Test_4_2_Space_Processing
{
	@Test
	public void Example4_1() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_4.1.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			Sequence block = document.getSequence("a key in a mapping at indentation level 0");
			assertNotNull(block);
			assertEquals("This sequence is also at indentation level 0.", block.getScalar(0).toString());
			assertEquals("Another entry in the sequence.", block.getScalar(1).toString());
			Sequence nested = block.getSequence(2);
			assertNotNull(nested);
			//assertEquals("This nested sequence must be indented at least to level 1.", nested.getScalar(0).toString());
			//assertEquals("Another entry in the nested sequence.", nested.getScalar(1).toString());
			//assertEquals(2, nested.size());
			assertEquals("Last entry in block sequence at indentation level 0.", block.getScalar(3).toString());
			assertEquals(4, block.size());
			assertEquals("at indentation level 0.", document.getScalar("second key in mapping").toString());
			assertEquals(2, document.size());
		}
	}

	@Test
	public void Example4_2() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_4.2.yml"))
		{
			Sequence document = new YamlReader(input).readDocument().asSequence();
			assertNotNull(document);
			assertEquals("This sequence is not indented.", document.getScalar(0).toString());
			Mapping map = document.getMapping(1);
			assertNotNull(map);
			assertEquals("further indented by four.", map.getScalar("inline-map").toString());
			assertEquals("is also further indented by four.", map.getScalar("this key").toString());
			Sequence seqKey = new Sequence();
			seqKey.add("nested sequence used as key");
			seqKey.add("indented by eight spaces");
			Mapping mapVal = map.getMapping(seqKey);
			assertNotNull(mapVal);
			assertEquals("used as value", mapVal.getScalar("nested map").toString());
			assertEquals("six spaces", mapVal.getScalar("indented by").toString());
			assertEquals(2, mapVal.size());
			assertEquals(3, map.size());
			Sequence seq = document.getSequence(2);
			assertEquals("inline-seq; further indented by three.", seq.getScalar(0).toString());
			assertEquals("second entry in nested sequence.", seq.getScalar(1).toString());
			assertEquals(2, seq.size());
			assertEquals("Last entry in top sequence.", document.getScalar(3).toString());
			assertEquals(4, document.size());
		}
	}

	@Test
	public void Example4_3() throws IOException
	{
		try (InputStream input = Resources.getFile("/yaml/1.0/Example_4.3.yml"))
		{
			Mapping document = new YamlReader(input).readDocument().asMapping();
			assertNotNull(document);
			assertEquals("contains three lines of text.\nThe third one starts with a\n# character. This isn't a comment.\n", document.getScalar("this").toString());
			assertEquals(1, document.size());
		}
	}
}
