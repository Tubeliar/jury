package org.yaml.jury;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class ParseStream
{
	private static final char[] B_CHAR = new char[]{'\r', '\n'};
	private static final char[] S_WHITE = new char[]{' ', '\t'};

	private static final char[] SKIP_WHITESPACE;

	static
	{
		SKIP_WHITESPACE = Arrays.copyOf(B_CHAR, B_CHAR.length + S_WHITE.length);
		System.arraycopy(S_WHITE, 0, SKIP_WHITESPACE, B_CHAR.length, S_WHITE.length);
	}

	private InputStreamReader input;
	private final List<Integer> peek = new ArrayList<>(2);
	private int column;
	private int line = 1;

	public ParseStream(InputStream input)
	{
		this.input = new InputStreamReader(input);
	}

	public int peekAfterWhitespace() throws IOException
	{
		boolean doAnother = true;
		int nextChar = -1;
		while (doAnother && (nextChar = peek()) != -1)
		{
			doAnother = false;
			for (char whitespace : SKIP_WHITESPACE)
			{
				if (nextChar == whitespace)
				{
					read();
					doAnother = true;
					break;
				}
			}
		}
		return nextChar;
	}

	public int read() throws IOException
	{
		int result = peek.isEmpty() ? readFromStream() : peek.remove(0);

		column++;

		if (result == '\r')
		{
			// Normalize line breaks (Spec 5.4)
			if (peek() == '\n')
			{
				// Discard the peeked newline
				peek.remove(0);
			}
			result = '\n';
		}

		if (result == '\n')
		{
			line++;
			column = 0;
		}

		return result;
	}

	public StringBuilder readBuilder() throws IOException
	{
		StringBuilder builder = new StringBuilder();
		int next = read();
		if (next >= 0) builder.append((char)next);
		return builder;
	}

	public int peek() throws IOException
	{
		return peek(1);
	}

	public int peek(int ahead) throws IOException
	{
		while (peek.size() < ahead)
		{
			peek.add(readFromStream());
		}
		return peek.get(ahead - 1);
	}

	private int readFromStream() throws IOException
	{
		// Handle encoding and multibyte characters
		// At some point...
		return input.read();
	}

	StringBuilder readUntil(Pattern pattern) throws IOException
	{
		StringBuilder buffer = new StringBuilder();
		int nextChar;
		while ((nextChar = read()) != -1)
		{
			buffer.append((char) nextChar);
			if (pattern.matcher(buffer).find())
			{
				return buffer;
			}
		}
		return buffer;
	}

	StringBuilder readUpToExcluding(Pattern characterPattern) throws IOException
	{
		StringBuilder buffer = new StringBuilder();
		int nextChar;
		while ((nextChar = peek()) != -1)
		{
			if (characterPattern.matcher(Character.toString((char) nextChar)).matches())
			{
				break;
			}
			else
			{
				buffer.append((char) read());
			}
		}
		return buffer;
	}

	/**
	 * Get column of the character that will be read next
	 */
	public int getColumn()
	{
		return column;
	}

	public int getLine()
	{
		return line;
	}

	public boolean reachedEof() throws IOException
	{
		return peek() < 0;
	}
}