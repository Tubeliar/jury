package org.yaml.jury;

public abstract class Node
{
	private String tag;

	public boolean isCollection()
	{
		return false;
	}

	public boolean isScalar()
	{
		return false;
	}

	public boolean isSequence()
	{
		return false;
	}

	public boolean isMapping()
	{
		return false;
	}

	public Collection asCollection()
	{
		return null;
	}

	public Scalar asScalar()
	{
		return null;
	}

	public Sequence asSequence()
	{
		return null;
	}

	public Mapping asMapping()
	{
		return null;
	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}

	public String getTag()
	{
		return tag;
	}

	protected String tabs(int amount)
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < amount; i++)
		{
			builder.append("   ");
		}
		return builder.toString();
	}

	@Override
	public String toString()
	{
		return toString(0);
	}

	public String toString(int indentation)
	{
		return tabs(indentation) + toString();
	}
}