package org.yaml.jury;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Sequence extends Collection
{
	private final List<Node> content = new ArrayList<>();

	@Override
	public boolean isSequence()
	{
		return true;
	}

	@Override
	public Sequence asSequence()
	{
		return this;
	}

	@Override
	public int size()
	{
		return content.size();
	}

	@Override
	public Iterator<Node> iterator()
	{
		return content.iterator();
	}

	public void add(Node node)
	{
		content.add(node);
	}

	public void add(String scalarValue)
	{
		content.add(new Scalar(scalarValue));
	}

	public Node get(int index)
	{
		return content.get(index);
	}

	public Scalar getScalar(int index)
	{
		Node node = content.get(index);
		return node == null ? null : node.asScalar();
	}

	public Collection getCollection(int index)
	{
		Node node = content.get(index);
		return node == null ? null : node.asCollection();
	}

	public Sequence getSequence(int index)
	{
		Node node = content.get(index);
		return node == null ? null : node.asSequence();
	}

	public Mapping getMapping(int index)
	{
		Node node = content.get(index);
		return node == null ? null : node.asMapping();
	}

	@Override
	public boolean equals(Object other)
	{
		if (!(other instanceof Sequence))
		{
			return false;
		}
		List<Node> exclude = new ArrayList<>();
		Sequence otherSequence = (Sequence) other;
		return equalsWithout(otherSequence, exclude);
	}

	// Do not use equals to test excluded sequences to avoid recursion
	private boolean equalsWithout(Sequence otherSequence, List<Node> exclude)
	{
		if (otherSequence == null)
		{
			return false;
		}
		if (content.size() != otherSequence.content.size())
		{
			return false;
		}
		addIfNew(this, exclude);
		addIfNew(otherSequence, exclude);
		for (int index = 0; index < content.size(); index++)
		{
			Node node1 = content.get(index);
			Node node2 = otherSequence.content.get(index);

			if (node1 == null)
			{
				if (node2 != null)
				{
					return false;
				}
			}
			else if (node1.isSequence())
			{
				if (addIfNew(node1, exclude) && !node1.asSequence().equalsWithout(node2.asSequence(), exclude))
				{
					return false;
				}
			}
			else if (!node1.equals(node2))
			{
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode()
	{
		java.util.Collection<Node> exclude = new ArrayList<>();
		return hashCodeWithout(exclude);
	}

	@Override
	// Do not use a hash based collection for exclusions to avoid recursion
	protected int hashCodeWithout(java.util.Collection<Node> excludedNodes)
	{
		addIfNew(this, excludedNodes);
		int hashCode = 1;
		for (Node node : content)
		{
			int codeAddition;
			if (node != null && !excludedNodes.contains(node))
			{
				codeAddition = node.isCollection() ? node.asCollection().hashCodeWithout(excludedNodes) : node.hashCode();
			}
			else
			{
				codeAddition = 0;
			}
			hashCode = 31 * hashCode + codeAddition;
		}
		return hashCode;
	}

	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		result.append('[');
		boolean firstEntry = true;
		for (Node node : content)
		{
			if (firstEntry) firstEntry = false;
			else result.append(", ");
			result.append(node.toString());
		}
		result.append(']');
		return result.toString();
	}

	@Override
	public String toString(int indentation)
	{
		if (content.isEmpty())
		{
			return tabs(indentation) + "[]";
		}
		StringBuilder result = new StringBuilder();
		result.append(tabs(indentation));
		result.append("[\n");
		for (Node node : content)
		{
			result.append(node.toString(indentation + 1));
			result.append("\n");
		}
		result.append(tabs(indentation));
		result.append("]");
		return result.toString();
	}
}