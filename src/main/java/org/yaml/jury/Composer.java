package org.yaml.jury;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Composer
{
	public static class Element
	{
		final Node node;
		final int column;
		final int line;
		final boolean isFlow;

		private Element(int line, int column, boolean isFlow, Node node)
		{
			this.node = node;
			this.line = line;
			this.column = column;
			this.isFlow = isFlow;
		}

		@Override
		public String toString()
		{
			StringBuilder result = new StringBuilder();
			result.append(isFlow ? "Flow" : "Blok");
			if (getClass() == KeyValuePair.class) result.append("Kvp");
			else if (node == null) result.append("Nul");
			else if (node.isMapping()) result.append("Map");
			else if (node.isSequence()) result.append("Seq");
			else if (node.isScalar()) result.append("Sca");
			result.append(":").append(line).append(":").append(column).append(":");
			result.append(node == null ? "!!null" : format(node));
			if (getClass() == KeyValuePair.class)
			{
				Node mappedValue = ((KeyValuePair)this).mappedValue;
				result.append(" : ").append(mappedValue == null ? "!!null" : format(mappedValue));
			}
			return result.toString();
		}

		private String format(Node input)
		{
			String tag = input.getTag() == null ? "" : "[" + input.getTag() + "] ";
			return tag + input.toString().replace("\n", "\\n");
		}
	}

	public static class KeyValuePair extends Element
	{
		final Node mappedValue;
		final boolean haveKey;

		private KeyValuePair(int line, int column, boolean isFlow)
		{
			// Empty key value pair
			super(line, column, isFlow, null);
			mappedValue = null;
			haveKey = false;
		}

		private KeyValuePair(int line, int column, boolean isFlow, Node key, Node value)
		{
			// Fully specified key value pair
			super(line, column, isFlow, key);
			mappedValue = value;
			haveKey = true;
		}
	}

	public static class Tag extends Element
	{
		final int indexInComposeStack;

		public Tag(int line, int column, boolean isFlow, String tag)
		{
			this(line, column, isFlow, tag, -1);
		}

		public Tag(int line, int column, boolean isFlow, String tag, int indexInComposeStack)
		{
			super(line, column, isFlow, new Scalar(tag));
			this.indexInComposeStack = indexInComposeStack;
		}

		public boolean isGlobal()
		{
			return indexInComposeStack >= 0;
		}
	}

	public static class Alias extends Element
	{
		public Alias(int line, int column, boolean isFlow, String alias)
		{
			super(line, column, isFlow, new Scalar(alias));
		}
	}

	private final Stack<Element> composeStack = new Stack<>();
	private final Stack<Tag> tagPrefixStack = new Stack<>();
	private final Map<String, Node> knownAnchors = new HashMap<>();
	private Element document;
	public boolean superVerbose;
	int column;
	private int line;
	private boolean flow;
	private String anchor;

	public boolean setPosition(int line, int column)
	{
		this.line = line;
		this.column = column;

		while (!composeStack.isEmpty() && !canBeParent(peek(), column))
		{
			logVerbose("Want column " + column);
			pop(true);
		}
		Element top = peek();
		flow = top != null && top.isFlow;
		return flow;
	}

	private boolean canBeParent(Element element, int indentation)
	{
		if (element == null)
		{
			return true;
		}
		if (element.isFlow)
		{
			return true;
		}
		if (element.node != null && (element.node.isCollection() || element instanceof KeyValuePair))
		{
			return element.column <= indentation;
		}
		return element.column < indentation;
	}

	public Node getRoot()
	{
		while (!composeStack.isEmpty()) pop(true);
		Node topNode = document == null ? null : document.node;
		logVerbose("++++");
		logVerbose("" + topNode);
		logVerbose("++++");
		return topNode == null ? new Scalar("") : topNode;
	}

	public boolean hasContent()
	{
		return !composeStack.isEmpty();
	}

	private void reportStack()
	{
		logVerbose("Stack has become:");
		for (int index = 0; index < composeStack.size(); index++)
		{
			logVerbose("" + composeStack.get(composeStack.size() - 1 - index));
		}
		if (composeStack.isEmpty()) logVerbose(" - empty -");
	}

	private void commit(Element element)
	{
		if (peek() == element)
		{
			Thread.dumpStack();
		}
		if (element == null)
		{
			logVerbose("No null plz!");
			return;
		}

		if (anchor != null)
		{
			logVerbose("Associate anchor " + anchor + " with " + element);
			if (knownAnchors.containsKey(anchor))
			{
				logWarning("Redefining existing anchor " + anchor);
			}
			knownAnchors.put(anchor, element.node);
			anchor = null;
		}

		if (element instanceof KeyValuePair)
		{
			Element top = peek();
			// A block sequence could be indented at the same level as a key
			// or there could be a key/value pair with a null value on top
			if (top != null && top.column == element.column && (
					top.node != null && top.node.isSequence() ||
					top instanceof KeyValuePair)
				)
			{
				logVerbose("Popping top to make room for mapping");
				pop(true);
				top = peek();
			}
			// Add a new mapping, but only if this is the first entry in it
			if (top == null || !top.isFlow && top.column != element.column || top.node == null || !top.node.isMapping())
			{
				logVerbose("Creating a new mapping for this kvp because top = " + top);
				composeStack.push(new Element(element.line, element.column, flow, new Mapping()));
			}
		}
		logVerbose("push " + element);
		composeStack.push(element);
		reportStack();
	}

	private Element pop(boolean andCombine)
	{
		// We have to remove one element but we also need to link it to the element below
		Element top = composeStack.pop();
		logVerbose("Popped: " + top);
		while (andCombine && !composeStack.isEmpty() && composeStack.get(composeStack.size() - 1) instanceof Tag)
		{
			Tag tag = (Tag) composeStack.pop();
			if (tag.isGlobal())
			{
				logVerbose("Popped global tag " + tag.node);
			}
			else
			{
				top.node.setTag(tag.node.toString());
				logVerbose("Add tag " + tag.toString());
			}
		}
		if (composeStack.isEmpty()) document = top;
		if (!andCombine) return top;
		Element nextElement = peek();
		if (nextElement == null)
		{
			if (!composeStack.isEmpty())
			{
				logWarning("can't combine with null element for " + top);
			}
		}
		else if (nextElement instanceof KeyValuePair)
		{
			if (((KeyValuePair)nextElement).haveKey)
			{
				replaceTop(new KeyValuePair(nextElement.line, nextElement.column, nextElement.isFlow, nextElement.node, top.node));
				logVerbose("Completed kvp: " + peek());
				pop(true); // Recursive call is going to commit the completed kvp to its mapping
			}
			else
			{
				replaceTop(new KeyValuePair(nextElement.line, nextElement.column, nextElement.isFlow, top.node, null));
			}
		}
		else if (nextElement.node == null)
		{
			logWarning("Can't use " + nextElement + " to combine with " + top);
		}
		else if (top instanceof KeyValuePair)
		{
			if (!nextElement.node.isMapping())
			{
				logWarning("There was supposed to be a mapping to put a key value pair in");
			}
			else
			{
				nextElement.node.asMapping().add(top.node, ((KeyValuePair)top).mappedValue);
				logVerbose("Put it in mapping " + nextElement);
			}
		}
		else if (nextElement.node.isSequence())
		{
			nextElement.node.asSequence().add(top.node);
			logVerbose("Put it in sequence " + nextElement);
		}
		else
		{
			logVerbose("Didn't know what to do with it.");
		}
		return top;
	}

	private void pop(Element goal)
	{
		Element current = null;
		while (current != goal && composeStack.size() > 1 && composeStack.contains(goal))
		{
			current = pop(true);
		}
	}

	private Element peek()
	{
		return peek(0);
	}

	private Element peek(int depth)
	{
		for (int index = composeStack.size() - 1; index >=0; index--)
		{
			Element element = composeStack.get(index);
			if (!(element instanceof Tag))
			{
				depth--;
				if (depth < 0) return element;
			}
		}
		return null;
	}

	private void replaceTop(Element newTop)
	{
		// Composer.pop() will integrate the popped element into the next element.
		// If we need to replace elements we avoid this mechanism.
		Element oldTop = pop(false);
		logVerbose("Replace " + oldTop + " with " + newTop);
		commit(newTop);
	}

	public void addScalar(StringBuilder content)
	{
		addScalar(content.toString());
	}

	public void addScalar(String content)
	{
		logVerbose("New scalar: " + content);
		commit(new Element(line, column, flow, new Scalar(content)));
	}

	public void newFlowEntry()
	{
		for (int amount = 1; amount <= composeStack.size(); amount++)
		{
			Element element = composeStack.get(composeStack.size() - amount);
			if (element.isFlow && element.node != null && element.node.isCollection())
			{
				logVerbose("next entry for " + element);
				if (amount > 1)
				{
					element = composeStack.get(composeStack.size() - amount + 1);
					pop(element);
				}
				return;
			}
		}
		logWarning("There is no active flow collection for a new entry");
	}

	public void startExplicitSequence()
	{
		commit(new Element(line, column, true, new Sequence()));
	}

	public void endExplicitSequence()
	{
		for (int amount = 1; amount <= composeStack.size(); amount++)
		{
			Element element = composeStack.get(composeStack.size() - amount);
			if (element.isFlow && element.node != null && element.node.isSequence())
			{
				logVerbose("End sequence " + element);
				pop(element);
				return;
			}
		}
		logWarning("There is no active flow sequence to end");
	}

	public void startExplicitMapping()
	{
		logVerbose("Starting a flow mapping");
		commit(new Element(line, column, true, new Mapping()));
	}

	public void endExplicitMapping()
	{
		for (int amount = 1; amount <= composeStack.size(); amount++)
		{
			Element element = composeStack.get(composeStack.size() - amount);
			if (element.isFlow && element.node != null && element.node.isMapping())
			{
				logVerbose("End mapping " + element);
				pop(element);
				return;
			}
		}
		logWarning("There is no active flow mapping to end");
	}

	public void addBlockSequence()
	{
		// Add a new sequence, but only if this is the first entry in it
		Element top = peek();
		if (top == null || top.column != column || top.node == null || !top.node.isSequence())
		{
			logVerbose("Creating a new sequence");
			commit(new Element(line, column, false, new Sequence()));
		}
	}

	public void addBlockMapping()
	{
		logVerbose("Starting a complex key");
		commit(new KeyValuePair(line, column, false));
	}

	public void addTag(String tag)
	{
		logVerbose("Adding tag " + tag);
		int caretPos = tag.indexOf('^');
		if (caretPos == 1)
		{
			boolean popTag = false;
			do
			{
				if (tagPrefixStack.isEmpty()) break;
				Tag topTag = tagPrefixStack.peek();
				int stackPos = topTag.indexInComposeStack;
				if (stackPos >= composeStack.size()) popTag = true;
				else if (composeStack.elementAt(stackPos) != topTag) popTag = true;
				if (popTag) tagPrefixStack.pop();
			}
			while (popTag);
			if (!tagPrefixStack.isEmpty())
			{
				tag = tagPrefixStack.peek().node.toString() + tag.substring(2);
				logVerbose("Found tag prefix. Tag is now " + tag);
			}
		}
		if (caretPos > 1)
		{
			logVerbose("Tag is a prefix");
			Tag globalTag = new Tag(line, column, flow, tag.substring(0, caretPos), composeStack.size());
			composeStack.push(globalTag);
			tagPrefixStack.push(globalTag);
			if (caretPos == tag.length()) return;
			tag = globalTag.node.toString() + tag.substring(caretPos + 1);
		}
		composeStack.push(new Tag(line, column, flow, tag));
	}

	public void addAnchor(String anchor)
	{
		this.anchor = anchor;
	}

	public void insertAlias(String alias)
	{
		if (!knownAnchors.containsKey(alias))
		{
			logWarning("Anchor not seen: " + alias);
		}
		else
		{
			logVerbose("Inserting alias " + alias);
			Node node = knownAnchors.get(alias);
			commit(new Element(line, column, flow, node));
		}
	}

	public void addPair()
	{
		Element key = peek();
		if (key == null) logWarning("Can't create a key from null");
		else if (key instanceof KeyValuePair && ((KeyValuePair)key).haveKey)
		{
			logVerbose("Already a key: " + key);
		}
		else
		{
			logVerbose("Making a key out of " + key);
			Element checkKvp = peek(1);
			if (checkKvp != null && checkKvp instanceof KeyValuePair && !((KeyValuePair)checkKvp).haveKey)
			{
				// Just popping the element will put it as a key in the underlying kvp
				pop(true);
			}
			else replaceTop(new KeyValuePair(key.line, key.column, key.isFlow, key.node, null));
		}
	}

	/**
	 * Returns the indentation for the top element if it is a key/value pair, otherwise -1.
	 * @return the indentation for the key if present.
	 */
	public int getKeyIndentation()
	{
		Element top = peek();
		if (!(top instanceof KeyValuePair)) return -1;
		return top.column;
	}

	/**
	 * Get the line of the last accepted element or 0 if there is no top element.
	 * This can differ from the line the reader or parser are on.
	 * @return The line of the last accepted element.
	 */
	public int getLine()
	{
		Element top = peek();
		return top == null ? 0 : top.line;
	}

	private void logVerbose(String message)
	{
		if (superVerbose)
		{
			System.err.println(message);
		}
	}

	private void logWarning(String message)
	{
		System.err.println(message);
	}
}