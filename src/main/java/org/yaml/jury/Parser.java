package org.yaml.jury;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

public class Parser
{
	private static final Pattern END_DOUBLE_QUOTE = Pattern.compile("[^\\\\]\"");
	private static final Pattern END_SINGLE_QUOTE = Pattern.compile("[^\\\\]'");
	private static final Pattern CC_END_LINE = Pattern.compile("[\n\r]");
	private static final Pattern CC_END_SCALAR = Pattern.compile("[:\n\r#]");
	private static final Pattern CC_END_FLOW_SCALAR = Pattern.compile("[:\\]},#]");
	private static final Pattern CC_END_TAG = Pattern.compile("[ \t\n\r]");
	private static final Pattern CC_WHITESPACE = Pattern.compile("[ \t\n\r]");

	static final Pattern B_CHAR_1_2 = Pattern.compile("[\n\r]");
	static final Pattern B_CHAR_1_0 = Pattern.compile("[\n\r\u0085\u2028\u2029]");

	private final ParseStream input;
	private boolean documentStarted;
	private boolean documentEnded;

	private enum Chomping
	{
		Strip,
		Clip,
		Keep
	}

	public Parser(InputStream input)
	{
		this.input = new ParseStream(input);
	}

	public Node parseDocument()
	{
		return parseDocument(false);
	}

	public Node parseDocument(boolean superVerbose)
	{
		Composer composer = new Composer();
		composer.superVerbose = superVerbose;
		documentStarted = false;
		documentEnded = false;
		int nextChar;
		try
		{
			while ((nextChar = input.peekAfterWhitespace()) != -1)
			{
				StringBuilder buffer = actOnCharacter(nextChar, composer);
				// We must use the buffer to read tokens. If the buffer is still empty
				// then a single character will have been used, but the while loop only
				// peeks. To make sure we advance we now consume the peeked character.
				if (buffer == null)
				{
					input.read();
				}
				if (documentEnded) break;
			}
		} catch (IOException e)
		{
			//...
		}
		return composer.getRoot();
	}

	private boolean handleDocumentStart(Composer composer) throws IOException
	{
		if (composer.column != 0) return false;
		for (int ahead = 1; ahead <= 3; ahead++)
		{
			if (input.peek(ahead) != '-') return false;
		}
		if (!matches(CC_WHITESPACE, input.peek(4))) return false;

		documentStarted |= composer.hasContent();
		documentEnded = documentStarted;
		documentStarted = true;

		for (int consume = 0; consume < 3; consume++) input.read();
		return true;
	}

	private boolean handleDocumentEnd(Composer composer, String line)
	{
		if (composer.column != 0) return false;

		if (line.startsWith("..."))
		{
			documentEnded = true;
			return true;
		}
		return false;
	}

	private StringBuilder actOnCharacter(int nextChar, Composer composer) throws IOException
	{
		StringBuilder buffer = null;
		boolean flowCollection = composer.setPosition(input.getLine(), input.getColumn());
		switch (nextChar)
		{
		case '"':
			buffer = input.readUntil(END_DOUBLE_QUOTE);
			String content = postProcessQuoted(buffer, true);
			composer.addScalar(Escapes.process(content));
			break;
		case '\'':
			buffer = input.readUntil(END_SINGLE_QUOTE);
			// Concatenate escaped single quotes
			while (input.peek() == '\'')
			{
				input.read();
				buffer.append(input.readUntil(END_SINGLE_QUOTE));
			}
			composer.addScalar(postProcessQuoted(buffer, false));
			break;
		case '%':
			buffer = input.readUpToExcluding(CC_END_LINE);
			// TODO: process directive
			break;
		case '#':
			// Comments must be thrown away
			input.readUpToExcluding(CC_END_LINE);
			break;
		case '!':
			buffer = input.readUpToExcluding(CC_END_TAG);
			composer.addTag(buffer.toString());
			break;
		case '&':
			input.read(); // Throw away the anchor indicator
			buffer = input.readUpToExcluding(CC_END_TAG);
			composer.addAnchor(buffer.toString());
			break;
		case '*':
			input.read(); // Throw away the alias indicator
			buffer = input.readUpToExcluding(CC_END_TAG);
			composer.insertAlias(buffer.toString());
			break;
		case '|':
		case '>':
			buffer = readBlockScalar(composer);
			composer.addScalar(buffer);
			break;
		case ',':
			composer.newFlowEntry();
			break;
		case '[':
			composer.startExplicitSequence();
			break;
		case ']':
			composer.endExplicitSequence();
			break;
		case '{':
			composer.startExplicitMapping();
			break;
		case '}':
			composer.endExplicitMapping();
			break;
		case '-':
			if (matches(CC_END_TAG, input.peek(2)))
			{
				composer.addBlockSequence();
			}
			else if (!handleDocumentStart(composer))
			{
				buffer = input.readBuilder();
				finishScalar(flowCollection, buffer, composer);
			}
			break;
		case '?':
			buffer = input.readBuilder();
			if (matches(CC_END_TAG, input.peek()))
			{
				composer.addBlockMapping();
			}
			else
			{
				finishScalar(flowCollection, buffer, composer);
			}
			break;
		case ':':
			buffer = input.readBuilder();
			if (matches(CC_END_TAG, input.peek()))
			{
				composer.addPair();
			}
			else
			{
				finishScalar(flowCollection, buffer, composer);
			}
			break;
		default:
			buffer = readScalar(flowCollection, composer);
			if (!handleDocumentEnd(composer, buffer.toString()))
			{
				composer.addScalar(buffer);
			}
			break;
		}
		return buffer;
	}

	private static boolean matches(Pattern pattern, int character)
	{
		return pattern.matcher(Character.toString((char) character)).matches();
	}

	private String postProcessQuoted(StringBuilder buffer, boolean processEscapes)
	{
		// Reading unquoted scalars is already quite involved with newline processing. To keep it from
		// having to deal with escaping and closing quotes on top of that we read quoted scalars to their
		// ending quote first and deal with newlines later.
		StringBuilder result = new StringBuilder();
		boolean previousWasEmpty = true;
		boolean previousEndedEscape = false;
		for (String line : B_CHAR_1_0.split(buffer))
		{
			String trimmed = line.trim();
			boolean currentEndsEscape = false;
			if (processEscapes && trimmed.endsWith("\\"))
			{
				trimmed = trimmed.substring(0, trimmed.length() - 1);
				currentEndsEscape = true;
			}
			if (trimmed.isEmpty()) result.append('\n');
			else
			{
				if (!previousWasEmpty && !previousEndedEscape) result.append(' ');
				result.append(trimmed);
			}
			previousWasEmpty = trimmed.isEmpty();
			previousEndedEscape = currentEndsEscape;
		}
		return noQuotes(result);
	}

	private void finishScalar(boolean flow, StringBuilder buffer, Composer composer) throws IOException
	{
		buffer.append(readScalar(flow, composer));
		if (!handleDocumentEnd(composer, buffer.toString()))
		{
			composer.addScalar(buffer);
		}
	}

	private StringBuilder readScalar(boolean flow, Composer composer) throws IOException
	{
		return flow ? readFlowScalar() : readBlockScalar(composer);
	}

	private StringBuilder readFlowScalar() throws IOException
	{
		StringBuilder result = new StringBuilder();
		boolean keepGoing = true;
		while (keepGoing)
		{
			StringBuilder nextPart = input.readUpToExcluding(CC_END_FLOW_SCALAR);
			result.append(nextPart);
			keepGoing = input.peek(1) == ':' && !matches(CC_WHITESPACE, input.peek(2));
			// If the next character could have ended the scalar but didn't then include it in the result
			// to make sure the next read won't immediately halt too
			if (keepGoing) result.append((char) input.read());
		}
		trimTrailing(result);
		return result;
	}

	private StringBuilder readBlockScalar(Composer composer) throws IOException
	{
		StringBuilder result = new StringBuilder();
		// Process block indicators. Quoted scalars are not read here, so anything
		// that does not have indicators must be a plain scalar.
		boolean literal = false;
		boolean isPlain = true;
		Chomping chomp = Chomping.Strip;
		switch (input.peek())
		{
			case '>':
				literal = false;
				isPlain = false;
				break;
			case '|':
				literal = true;
				isPlain = false;
				break;
		}
		if (!isPlain)
		{
			// We have the block style indicator. Now consume it so we can have a look at the rest.
			input.read();
			switch (input.peek())
			{
				case '-': chomp = Chomping.Strip; break;
				case '+': chomp = Chomping.Keep; break;
				default:  chomp = Chomping.Clip; break;
			}
			// If we have an indicator then the content starts on the next line.
			// Peek to get the right indentation..
			input.readUntil(CC_END_LINE);
			input.peekAfterWhitespace();
			composer.setPosition(input.getLine(), input.getColumn());
		}
		Pattern endPattern =isPlain ? CC_END_SCALAR : CC_END_LINE;
		int blockIndentation = input.getColumn();
		if (input.getLine() == composer.getLine())
		{
			int keyIndentation = composer.getKeyIndentation();
			if (keyIndentation >= 0) blockIndentation = keyIndentation + 1;
		}
		int lineIndentation = input.getColumn();
		boolean wasIndented;
		boolean isIndented = false;
		int firstLine = input.getLine();
		int lastLine = input.getLine();
		int lastContentLine = input.getLine();
		boolean keepGoing = true;
		while (keepGoing)
		{
			StringBuilder nextPart = input.readUpToExcluding(endPattern);
			if (lineIndentation == 0 && handleDocumentEnd(composer, nextPart.toString()))
			{
				break;
			}
			else
			{
				result.append(nextPart);
				lastContentLine = input.getLine();
			}

			// We are now at the end of a line, the end of a key or potential start of a comment
			int next = input.peekAfterWhitespace();
			keepGoing = next >= 0 && (input.getLine() == lastLine || input.getColumn() >= blockIndentation);
			if (isPlain)
			{
				if (next == ':') keepGoing = !matches(CC_WHITESPACE, input.peek(2));
				else if (next == '#') keepGoing = false;
			}

			if (keepGoing)
			{
				// This scalar is not complete yet. Deal with line boundaries if we encounter them.
				if (input.getLine() != lastLine)
				{
					if (isPlain) trimTrailing(result);
					lineIndentation = input.getColumn();
					boolean haveBlankLines = input.getLine() - lastLine > 1;
					wasIndented = isIndented;
					isIndented = lineIndentation > blockIndentation;
					for (int blankLines = lastLine + 1; blankLines < input.getLine(); blankLines++) result.append('\n');
					if (wasIndented && !isIndented) result.append('\n');
					if (literal || isIndented && !isPlain)
					{
						result.append('\n');
						for (int indent = blockIndentation; indent < lineIndentation; indent++) result.append(' ');
					}
					else if (!haveBlankLines) result.append(' ');
				}
				// We must include the character that stopped us before, or we'll be stopped again and get caught in a loop.
				result.append((char) input.read());
				lastContentLine = input.getLine();
			}
			lastLine = input.getLine();
		}
		if (lastLine == firstLine) trimTrailing(result);
		int addNewlines = input.reachedEof() && lastLine != lastContentLine ? 1 : 0;
		switch (chomp)
		{
			case Keep:
				addNewlines += lastLine - lastContentLine;
				break;

			case Clip:
				addNewlines = Math.min(1, addNewlines + lastLine - lastContentLine);
				break;

			case Strip:
				addNewlines = 0;
				break;
		}
		for (int addOne = 0; addOne < addNewlines; addOne++) result.append('\n');
		return result;
	}

	private String noQuotes(StringBuilder buffer)
	{
		return buffer.substring(1, buffer.length() - 1);
	}

	private void trimTrailing(StringBuilder buffer)
	{
		while (buffer.length() > 0 && matches(CC_WHITESPACE, buffer.charAt(buffer.length() - 1)))
		{
			buffer.setLength(buffer.length() - 1);
		}
	}
}