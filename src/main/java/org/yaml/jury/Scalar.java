package org.yaml.jury;

public class Scalar extends Node
{
	private final String content;

	public Scalar(String content)
	{
		this.content = content;
	}

	public Scalar(StringBuilder content)
	{
		this.content = content.toString();
	}

	@Override
	public boolean isScalar()
	{
		return true;
	}

	@Override
	public Scalar asScalar()
	{
		return this;
	}

	public int asInt()
	{
		try
		{
			return Integer.parseInt(content);
		} catch (Exception e)
		{
			return 0;
		}
	}

	public double asDouble()
	{
		try
		{
			return Double.parseDouble(content);
		} catch (Exception e)
		{
			return Double.NaN;
		}
	}

	@Override
	public boolean equals(Object other)
	{
		if (!(other instanceof Scalar))
		{
			return false;
		}
		Scalar otherScalar = (Scalar) other;
		if (content == null)
		{
			return otherScalar.content == null;
		}
		return content.equals(otherScalar.content);
	}

	@Override
	public int hashCode()
	{
		if (content == null)
		{
			return 0;
		}
		return content.hashCode();
	}

	@Override
	public String toString()
	{
		return content;
	}
}