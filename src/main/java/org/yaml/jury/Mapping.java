package org.yaml.jury;

import java.util.*;

public class Mapping extends Collection
{
	private final Map<Node, Node> content = new LinkedHashMap<>();

	@Override
	public boolean isMapping()
	{
		return true;
	}

	@Override
	public Mapping asMapping()
	{
		return this;
	}

	@Override
	public int size()
	{
		return content.size();
	}

	@Override
	public Iterator<Node> iterator()
	{
		return content.values().iterator();
	}

	public void add(Node key, Node value)
	{
		content.put(key, value);
	}

	public Node get(Node key)
	{
		return content.get(key);
	}

	public Scalar getScalar(Node key)
	{
		Node node = content.get(key);
		return node == null ? null : node.asScalar();
	}

	public Collection getCollection(Node key)
	{
		Node node = content.get(key);
		return node == null ? null : node.asCollection();
	}

	public Sequence getSequence(Node key)
	{
		Node node = content.get(key);
		return node == null ? null : node.asSequence();
	}

	public Mapping getMapping(Node key)
	{
		Node node = content.get(key);
		return node == null ? null : node.asMapping();
	}

	public Node get(String scalarKey)
	{
		return get(new Scalar(scalarKey));
	}

	public Scalar getScalar(String scalarKey)
	{
		return getScalar(new Scalar(scalarKey));
	}

	public Collection getCollection(String scalarKey)
	{
		return getCollection(new Scalar(scalarKey));
	}

	public Sequence getSequence(String scalarKey)
	{
		return getSequence(new Scalar(scalarKey));
	}

	public Mapping getMapping(String scalarKey)
	{
		return getMapping(new Scalar(scalarKey));
	}

	public Set<Node> keySet()
	{
		return content.keySet();
	}

	@Override
	public boolean equals(Object other)
	{
		if (!(other instanceof Mapping))
		{
			return false;
		}
		Mapping otherMapping = (Mapping) other;
		return content.size() == otherMapping.content.size()
				&& content.keySet().stream().noneMatch(key -> !otherMapping.content.containsKey(key) || !content.get(key).equals(otherMapping.content.get(key)));
	}

	@Override
	public int hashCode()
	{
		java.util.Collection<Node> exclude = new ArrayList<>();
		return hashCodeWithout(exclude);
	}

	@Override
	protected int hashCodeWithout(java.util.Collection<Node> excludedNodes)
	{
		if (!excludedNodes.contains(this))
		{
			excludedNodes.add(this);
		}
		int hashCode = 0;
		for (Map.Entry<Node, Node> entry : content.entrySet())
		{
			Node key = entry.getKey();
			Node value = entry.getValue();
			// Order does not matter in a map, so we use xor as our main calculation.
			// However, for complex nested maps where some collections appear both as key and value
			// the order will matter because they will be encountered and excluded in a different order.
			int keyHash = 0;
			if (key != null && !excludedNodes.contains(key))
			{
				keyHash = key.isCollection() ? key.asCollection().hashCodeWithout(excludedNodes) : key.hashCode();
			}
			int valueHash = 0;
			if (value != null && !excludedNodes.contains(value))
			{
				valueHash = value.isCollection() ? value.asCollection().hashCodeWithout(excludedNodes) : value.hashCode();
			}
			hashCode = hashCode ^ (keyHash << 16 | valueHash);
		}
		return hashCode;
	}

	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		result.append('{');
		boolean firstEntry = true;
		for (Map.Entry<Node, Node> entry : content.entrySet())
		{
			if (firstEntry) firstEntry = false;
			else result.append(", ");
			result.append(entry.getKey());
			result.append('=');
			result.append(entry.getValue());
		}
		result.append('}');
		return result.toString();
	}

	@Override
	public String toString(int indentation)
	{
		if (content.isEmpty())
		{
			return tabs(indentation) + "{}";
		}
		StringBuilder result = new StringBuilder();
		result.append(tabs(indentation));
		result.append("{\n");
		for (Map.Entry<Node, Node> entry : content.entrySet())
		{
			result.append(entry.getKey().toString(indentation + 1));
			result.append(": ");
			result.append(entry.getValue().toString());
			result.append("\n");
		}
		result.append(tabs(indentation));
		result.append("}");
		return result.toString();
	}
}