package org.yaml.jury;

import java.io.InputStream;

/**
 * A minimal implementation of a yml reader, mostly to be able to read openCV output
 * Both SnakeYAML and YAMLBeans choke on the %YAML:1.0 directive and try to
 * translate !!-types to java classes which is inconvenient if you just want the data.
 * This reader is very lenient and accepts some non conformant data.
 */
public class YamlReader
{
	final Parser parser;

	public YamlReader(InputStream input)
	{
		parser = new Parser(input);
	}

	public Node readDocument()
	{
		return parser.parseDocument();
	}

	public Node readDocument(boolean superVerbose)
	{
		return parser.parseDocument(superVerbose);
	}
}