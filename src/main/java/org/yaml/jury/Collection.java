package org.yaml.jury;

public abstract class Collection extends Node implements Iterable<Node>
{
	@Override
	public boolean isCollection()
	{
		return true;
	}

	@Override
	public Collection asCollection()
	{
		return this;
	}

	protected abstract int hashCodeWithout(java.util.Collection<Node> excludedNodes);

	// Add without using equals, for use in overriding equals while avoiding recursion
	protected boolean addIfNew(Node node, java.util.Collection<Node> collection)
	{
		for (Node conatinedNode : collection)
		{
			if (node == conatinedNode)
			{
				return false;
			}
		}
		collection.add(node);
		return true;
	}

	public abstract int size();
}