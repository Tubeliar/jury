package org.yaml.jury;

public class Escapes
{
	private static final int BITS_IN_BYTE = 8;
	private static final int HEX = 16;

	public static StringBuilder process(String text)
	{
		StringBuilder buffer = new StringBuilder();
		int searchIndex = 0;
		int escapeIndex;
		while ((escapeIndex = text.indexOf('\\', searchIndex)) >= 0)
		{
			// Copy the part before the escape
			if (escapeIndex > searchIndex)
			{
				buffer.append(text.substring(searchIndex, escapeIndex));
			}

			// the string should not end on an esape sequence, but we allow it
			if (escapeIndex + 1 >= text.length())
			{
				buffer.append('\\');
				break;
			}

			searchIndex = process(text, buffer, escapeIndex);
		}
		buffer.append(text.substring(searchIndex, text.length()));
		return buffer;
	}

	private static int process(String text, StringBuilder buffer, int escapeIndex)
	{
		// Section 4.6.9 of spec 1.0
		char escapeChar = text.charAt(escapeIndex + 1);
		int nChars = 0;
		switch (escapeChar)
		{
			case '\\':
				buffer.append('\\');
				break;
			case '\"':
				buffer.append('\"');
				break;
			case 'a':
				// BEL
				buffer.append((char) 0x7);
				break;
			case 'b':
				buffer.append('\b');
				break;
			case 'e':
				// escape
				buffer.append((char) 0x1B);
				break;
			case 'f':
				buffer.append('\f');
				break;
			case 'n':
				buffer.append('\n');
				break;
			case 'r':
				buffer.append('\r');
				break;
			case 't':
				buffer.append('\t');
				break;
			case 'v':
				// vertical tab
				buffer.append((char) 0xB);
				break;
			case '^':
				buffer.append('^');
				break;
			case '0':
				buffer.append('\0');
				break;
			case ' ':
				buffer.append(' ');
				break;
			case '_':
				buffer.append('\u00A0');
				break;
			case 'N':
				buffer.append('\u0085');
				break;
			case 'L':
				buffer.append('\u2028');
				break;
			case 'x':
				nChars = 2;
				buffer.append((char) hexVal(text, escapeIndex + 2, nChars));
				break;
			case 'u':
				nChars = 4;
				buffer.append((char) hexVal(text, escapeIndex + 2, nChars));
				break;
			case 'U':
				nChars = 8;
				int twoChar = hexVal(text, escapeIndex + 2, nChars);
				buffer.append((char) ((twoChar >> BITS_IN_BYTE) & 0xFFFF));
				buffer.append((char) (twoChar & 0xFFFF));
				break;
			default:
				if (Parser.B_CHAR_1_0.matcher(Character.toString(escapeChar)).matches())
				{
					// escaped line breaks are ignored by the spec
					break;
				}
				// Invalid escape character. We could generate an error
				buffer.append('\\');
				buffer.append(escapeChar);
		}
		// Add one for the escape itself and one for the escape character
		return escapeIndex + nChars + 2;
	}

	private static int hexVal(String text, int startIndex, int nChars)
	{
		if (startIndex >= text.length())
		{
			return 0;
		}
		String valueString = text.substring(startIndex, Math.min(text.length(), startIndex + nChars));
		return Integer.parseInt(valueString, HEX);
	}
}
