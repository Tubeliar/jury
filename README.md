# **Ju**st **R**ead **Y**AML

## Installation

This project is not on Maven central, so download or clone this repository and build it locally with `mvn install`. After that you can include it in your project as a dependency:

```xml
<dependency>
	<groupId>org.yaml</groupId>
	<artifactId>jury</artifactId>
	<version>1.0-SNAPSHOT</version>
</dependency>
```

## Usage

Import the relevant classes or the whole package: `import org.yaml.jury.*`

Create a reader and read a document:

```java
YamlReader reader = new YamlReader(new FileInputStream(name));
Node data = reader.readDocument();
```

Everything that JuRY returns is a Node. You can convert it to specific node types if you know what they are:

```java
if (!data.isMapping())
{
  System.out.println("That's not a mapping!");
}
Mapping map = data.asMapping();
String name = map.get("name").asScalar().toString();
double pi = map.get("pi").asScalar().asDouble();
Sequence fib = map.get("fibonacci").asSequence();
int third = fib.get(0).asScalar().asInt() + fib.get(1).asScalar().asInt();
```

## What does it do?

Just Read YAML. And not even fully at that. The current implementation is enough for it to be useful for myself, which is to read OpenCV files such as lens calibrations and other data files. JuRY is not feature complete. Some things such as tags might be lost and references are not handled. It also does not generate YML, not even from its own Nodes.

## Did we really need another YAML parser?

Existing parsers don't like the `YAML:1.0` tag at the beginning of some YAML files. Specifically files written by OpenCV contain them. You could hack your own reader that throws away the first line and passes the rest to a YAML reader of choice, but they all seem to insist on converting the data to objects when they encounter a `!!class.type` tag. Which is useful unless you just want the data and not a deserialization.

JuRY views YAML files as data that you want to process yourself. You could build your own deserializer on top of it if you want, but in those cases one of the other YAML readers might suit your needs better. I'm sure some of them also support a "just data" mode, but JuRY does it out of the box.